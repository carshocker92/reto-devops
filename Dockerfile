FROM node:12.22.1-alpine3.11

WORKDIR /app

COPY . .

RUN npm install

RUN addgroup -S app &&\
    adduser -s /bin/bash \
        -S -G app app \
    -h /home/app 
    

USER app

CMD ["node","/app/index.js"]